#!/usr/bin/python
import turtle

SIDE = 100
ANGLE = 90
EYE_SIZE = 5
MIDPOSX = 0
MIDPOSY = 0

brad = turtle.Turtle()

def draw_square():
  window = turtle.Screen()
  window.bgcolor("red")
  
  for i in range(0,25,1):
    j=0
    for j in range(0,4,1):
      brad.forward(SIDE)
      brad.right(ANGLE)
    brad.right(0.2)
  
  brad.penup()
  position = brad.position()
#print position
  
  # move to quater of the width and quater of the height
  brad.forward(SIDE* 0.25)
  brad.right(ANGLE)
  brad.forward(SIDE * 0.25)
  brad.pendown()
  
  
  #Draw the left eye
  brad.dot(EYE_SIZE,"black")
#brad.penup()
  brad.home()
  brad.pendown()

  #move to the 3rdquater of the widh and 3rd quater of the height
  #Draw the right eye
  brad.forward(SIDE * 0.75)
  brad.right(ANGLE)
  brad.forward(SIDE * 0.25)
  brad.pendown()
  brad.dot(EYE_SIZE, "black")
# brad.penup()
#back to home
  brad.goto(100,0)
  brad.penup()
  brad.home()
  
#Now time to draw the smily
  
  #left_jigle()
# middle_jigle()
  right_jigle()
  draw_nose()
      
  window.exitonclick()


def left_jigle():
  #move to the left jigle position
  #what about 0.4 times forward movement? roger!
  brad.forward(SIDE * 0.3)
  brad.right(ANGLE)
  brad.forward(SIDE * 0.7)
  brad.pendown()
  for i in range(0,10,1):
    brad.dot(EYE_SIZE, "Yellow")
    xpos,ypos = brad.pos()
    print xpos, ypos
    xpos += 1.00
    ypos -= 1.0
    brad.setx(xpos)
    brad.sety(ypos)
    brad.penup()
    midposx,midposy = xpos,ypos
  return midposx, midposy 

def middle_jigle():
  # go home!
  xpos , ypos = left_jigle()
  brad.home()
  brad.goto(xpos, ypos)
  for i in range(0,20,1):
    brad.dot(EYE_SIZE,"GREEN")
    xpos, ypos = brad.pos()
    xpos += 1.00
    brad.setx(xpos)
    midposx,midposy = xpos,ypos
  return midposx, midposy

def right_jigle():
  xpos, ypos = middle_jigle()
  brad.goto(xpos,ypos)
  for i in range(0,10,1):
    brad.dot(EYE_SIZE, "PURPLE")
    xpos,ypos = brad.pos()
    xpos += 1.00
    ypos += 1.00
    brad.setx(xpos)
    brad.sety(ypos)
  brad.home()
  brad.hideturtle()

def draw_nose():
  brad.penup()
  brad.home()
# get to the middle of the face
  brad.forward(SIDE * 0.5)
  brad.right(ANGLE)
  brad.forward(SIDE *0.5)
  brad.pendown()
  for i in range(0,3,1):
    brad.right(120)
    brad.forward(5)

  
draw_square()

