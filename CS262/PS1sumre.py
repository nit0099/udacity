#!/usr/bin/python

import re

def sumnums(sentence):
  sum = 0
  for each in re.findall(r"[0-9]+",sentence):
    sum = sum + int(each)
  return sum


test_case_input = """The Act of Independence of Lithuania was signed 
on February 16, 1918, by 20 council members."""

test_case_output = 1954

if sumnums(test_case_input) == test_case_output:
  print "Test case passed."
  print sumnums(test_case_input)
else:
  print "Test case failed:"
  print sumnums(text_case_input)
