#!/usr/bin/python

edges = {(1,'a') : 2,
         (2,'a') : 2,
         (2,'1') : 3,
         (3,'1') : 3
}

accepting = [3]

def fsmsim(string,current,edges,accepting):
  if string == "":
    return current in accepting
  else:
    letter = string[0]
    if (current,letter) in edges:
      destination = edges[(current,letter)]
      remaining_string = string[1:]
      return fsmsim(remaining_string, destination, edges, accepting)
    else:
      return False

start = 1
string = raw_input("Enter a string to be fsmed")
print fsmsim(string, start, edges, accepting)

