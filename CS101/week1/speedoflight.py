#!/usr/bin/python

speed_of_light = 299792458   # meters per second
meter = 100                  # one meter is 100 centimeters
nanosecond = 1.0/1000000000  # one billionth of a second


distance_travelled = speed_of_light*meter*nanosecond
print distance_travelled
